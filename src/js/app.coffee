window.App =
	run: ->
		$ = require('jquery')
		window.jQuery ?= $

		Suggest = require('sk-suggest')
		SuggestMenu = require('./SuggestMenu')

		$('.js-suggest')
			.each(->
				$menu = $('<div>').insertAfter(this)
				suggest = new Suggest(this).init()
				suggestMenu = new SuggestMenu($menu, suggest).init()

				suggestMenu
					.on('menuselect', (payload) ->
						console.log('select', payload)
					)

			)

		return

