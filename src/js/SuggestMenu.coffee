$ = require('jquery')
Events = require('sk-events')

class SuggestMenu extends Events
	constructor: (element, Suggest, options) ->
		super
		@$element = if element.jquery then element else $(element)
		@Suggest = Suggest
		@options = $.extend({
			item: 'li'
			selectedClass: 'is-selected'
		}, options)
		@$items = $([])
		@selectedIndex = 0
		@size = 0
		@isOpen = false
		@isInit = false

	init: ->
		if @isInit or not @$element.length or not @Suggest?
			return this

		@$element
			.hide()
			.on('mouseenter', @options.item, @handleMouseEnter)
			.on('mousedown', @options.item, @handleMouseDown)
			.on('click', @options.item, @handleClick)

		@Suggest
			.on('suggeststart', @handleSuggestStart)
			.on('suggestend', @handleSuggestEnd)
			.on('success', @handleSuccess)
			.on('typestart', @handleTypeStart)
			.on('typeclear', @handleTypeClear)
			.init()

		@isInit = true
		return this

	destroy: ->
		unless @isInit
			return this

		@$element
			.hide()
			.empty()
			.off('mouseenter', @options.item, @handleMouseEnter)
			.off('mousedown', @options.item, @handleMouseDown)
			.off('click', @options.item, @handleClick)

		@Suggest
			.off('suggeststart', @handleSuggestStart)
			.off('suggestend', @handleSuggestEnd)
			.off('success', @handleSuccess)
			.off('typestart', @handleTypeStart)
			.off('typeclear', @handleTypeClear)
			.destroy()

		@isInit = false
		return this

	open: ->
		unless @isOpen and @$element.is(':empty')
			@trigger('beforeopen')
			@$element.fadeIn(250, =>
				@trigger('afteropen')
			)
			@isOpen = true

	hide: (fastHide) ->
		if @isOpen
			@trigger('beforehide')
			if fastHide
				@$element.hide()
				@trigger('afterhide')
			else
				@$element.fadeOut(250, =>
					@trigger('afterhide')
				)
			@isOpen = false

	handleSuggestStart: =>
		if @Suggest.isResult
			@clearIndex()
			@open()

	handleSuggestEnd: =>
		# odděleno zkrz vyhledávání bez focosu, u menu nemá smysl
		@Suggest.typeTimer = clearTimeout(@Suggest.typeTimer)
		@Suggest.ajaxAbort()
		@Suggest.$input.removeClass(@Suggest.options.loadingClass)
		@hide()

	handleSuccess: (payload) =>
		@trigger('content',
			respond: payload.respond
		)
		@$element.html(payload.respond)

		@$items = @$element.find(@options.item)
		@size = @$items.length + 1

		@clearIndex()

		if @$element.is(':empty')
			@hide(true)
		else
			@open()

		@trigger('aftercontent')

	handleTypeStart: (payload) =>
		event = payload.event
		# console.log(payload)
		if event.which is 38
			@handleArrUp(event)
		else if event.which is 40
			@handleArrDown(event)
		else if event.which is 13 and @selectedIndex
			@handleEnter(event)

	handleTypeClear: =>
		@hide()
		@clearIndex()

	handleMouseEnter: (event) =>
		if @isOpen
			@setIndex(@$items.index(event.currentTarget) + 1, 'enter')

	setIndex: (index, type) ->
		o = @options
		if index isnt @selectedIndex
			@$items.removeClass(o.selectedClass)

			if index
				@$items
					.eq(index - 1)
					.addClass(o.selectedClass)
			# Scroll to invisible item if keys
			if index and type isnt 'enter'
				scroll = @$element.scrollTop()
				top = scroll + @$items.eq(index - 1).position().top
				height = @$element.height()

				if top < scroll or top > scroll + height
					@$element.scrollTop(top)

			@selectedIndex = index
			@trigger('menuchange')

	select: ->
		@trigger('menuselect',
			$item: @$items.eq(@selectedIndex - 1)
		)

	clearIndex: ->
		@$items.removeClass(@options.selectedClass)
		@selectedIndex = 0

	handleArrUp: (event) ->
		event.preventDefault()
		@setIndex((@selectedIndex - 1 + @size) % @size, 'up')

	handleArrDown: (event) ->
		event.preventDefault()
		@setIndex((@selectedIndex + 1) % @size, 'down')

	handleEnter: (event) ->
		event.preventDefault()
		@select()

	handleMouseDown : (event) =>
		event.preventDefault()
		@setIndex(@$items.index(event.currentTarget) + 1, 'click')
		@select()

	handleClick: (event) ->
		event.preventDefault()


module.exports = SuggestMenu
